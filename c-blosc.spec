Name:           c-blosc
Version:        1.21.6
Release:        3
Summary:        Binary compressor
License:        BSD-2-Clause AND BSD-3-Clause AND MIT
URL:            https://github.com/Blosc/c-blosc
Source:         https://github.com/Blosc/c-blosc/archive/v%{version}/blosc-%{version}.tar.gz
Patch0:         c-blosc-1.21.6-port-to-newer-cmake.patch

BuildRequires:  cmake lz4-devel snappy-devel zlib-devel libzstd-devel gcc-g++

%description
Blosc is a compression library that is faster than traditional compression.

%package        -n blosc
Summary:        Binary compressor

%description    -n blosc
Blosc is a compression library that is faster than traditional compression.

%package        -n blosc-devel
Summary:        Header files and libraries for Blosc development
Requires:       blosc = %{version}-%{release}

%description    -n blosc-devel
The blosc-devel package contains the header files and libraries needed
to develop programs that use the blosc meta-compressor.

%package        -n blosc-bench
Summary:        Benchmark for the Blosc compressor
Requires:       blosc = %{version}-%{release}
Requires:       python3-matplotlib

%description    -n blosc-bench
The blosc-bench package contains a benchmark suite which evaluates
the performance of Blosc, and compares it with memcpy.

%prep
%autosetup -n %{name}-%{version} -p1
rm -rf internal-complibs

#rm -r internal-complibs/snappy* internal-complibs/zlib*
sed -i '1i  set\(CMAKE_SKIP_RPATH true\)' bench/CMakeLists.txt
sed -i '1i  set\(CMAKE_POSITION_INDEPENDENT_CODE TRUE\)' CMakeLists.txt
sed -i '1i  #!/usr/bin/python3' bench/plot-speeds.py

%build
%cmake \
    -DCMAKE_BUILD_TYPE:STRING="Debug" \
    -DBUILD_STATIC:BOOL=OFF \
    -DPREFER_EXTERNAL_LZ4:BOOL=ON \
    -DTEST_INCLUDE_BENCH_SUITE:BOOL=OFF \
    -DPREFER_EXTERNAL_SNAPPY:BOOL=ON \
    -DPREFER_EXTERNAL_ZLIB:BOOL=ON \
    -DPREFER_EXTERNAL_ZSTD:BOOL=ON \
%cmake_build

%check
export LD_LIBRARY_PATH=%{buildroot}%{_libdir}
%ctest

%install
%cmake_install

install -p %{__cmake_builddir}/bench/bench -D %{buildroot}/%{_bindir}/blosc-bench
install -p bench/plot-speeds.py %{buildroot}/%{_bindir}/blosc-plot-times

%files -n blosc
%license LICENSES/*
%{_libdir}/libblosc.so.1*
%doc README.md ANNOUNCE.rst RELEASE_NOTES.rst README_CHUNK_FORMAT.rst 
%doc README_THREADED.rst COMPILING_WITH_WHEELS.rst CONTRIBUTING.md code_of_conduct.md

%files -n blosc-devel
%{_libdir}/libblosc.so
%{_libdir}/pkgconfig/blosc.pc
%{_includedir}/blosc.h
%{_includedir}/blosc-export.h

%files -n blosc-bench
%doc bench/*.c
%{_bindir}/blosc-bench
%{_bindir}/blosc-plot-times

%changelog
* Sat Mar 01 2025 Funda Wang <fundawang@yeah.net> - 1.21.6-3
- try build with cmake 4.0

* Mon Jan 27 2025 Funda Wang <fundawang@yeah.net> - 1.21.6-2
- adopt to cmake macro migration
- rename package so that it could be catched

* Thu Jan 02 2025 yaoxin <1024769339@qq.com> - 1.21.6-1
- Update to 1.21.6
  * Zlib updated to 1.3.1
  * Zstd updated to 1.5.6
  * Fixed many typos

* Wed Oct 30 2024 xuezhixin <xuezhixin@uniontech.com> - 1.21.5-2
- use the new cmake macros

* Wed Sep 27 2023 yaoxin <yao_xin001@hoperun.com> - 1.21.5-1
- Upgrade to 1.21.5

* Fri Jul 09 2021 xuguangmin <xuguangmin@kylinos.cn> - 1.21.0-1
- Update version to 1.21.0

* Mon May 31 2021 baizhonggui <baizhonggui@huawei.com> - 1.14.4-4
- Fix building error: No CMAKE_CXX_COMPULER could be found
- Add gcc-g++ in BuildRequires

* Thu Nov 14 2019 wangye<wangye54@huawei.com> - 1.14.4-3
- Update
* Thu Nov 14 2019 wangye<wangye54@huawei.com> - 1.14.4-2
- Package init
